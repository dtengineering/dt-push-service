# DT PUSH SERVICE
The application will send push notification to enterprise mobile app users.

# Building Artifact

```gradle bootJar```

# Building Docker Image

```docker build -t dt-push-service --build-arg JAR_FILE=build/libs/dt-push-service-1.0.0.jar .```

# Running dt-push-service locally

```docker run -d -p 8080:8080 --name dt-push-service dt-push-service```

# Stopping dt-push-service locally

```docker stop dt-push-service```

# API

## SEND PUSH NOTIFICATION

- METHOD  :   POST
- URL :   http://localhost:8080/api/v1/push
- CONTENT-TYPE  :    application/json
- PAYLOAD (REQUEST)  :   
```
{
  "createdTime":"2020-05-11 20:40:01",
  "message":"",
  "messageList": [
   {
    "message":"Hi Karthik, You received 5 out of 5 for new survey",
    "questions": [
            {
                "scale": "5",
                "title": "test question",
                "answer": "5"
            },
            {
                "scale": "10",
                "title": "test question 1",
                "answer": "10"
            } 
        ] 
     }
  ],
  "respondentId":"j9640ol5eg1e0fi9e3ubq4dtrc",
  "respondentName":"Someone",
  "surveyName":"DT_test",
  "surveyUUID":"86ae45ea-5960-426a-8d9b-01c6d8711c52",
  "userEmails": [
    "arone@dropthought.com"
  ],
  "userIds": [
    "1"
  ],
  "deepLink" : "",
  "appName"  : ""
}
```
- PAYLOAD (RESPONSE)  : 
```
{
  "result": {
    "failCount": 0,
    "successCount": 1
  },
  "success": true
}
```

- REQUEST PAYLOAD

| Fields   |      Type      |  Mandatory | 
|----------|:-------------:|------:|
| createdTime |  string | yes |      
| messageList |  List<Map>   |   yes |      
| respondentId   | string |    yes |      
| respondentName   | string |    yes | 
| surveyName   | string |    yes | 
| surveyUUID   | string |    yes | 
| userEmails   | List<String> |    yes | 
| userIds   | List<String> |    yes | 
| respondentId   | string |    yes |  
| deepLink   | string |    No |  
| appName   | string |    No |  

## Push Notification Bean

```
{
  "createdTime": "string",
  "messageList": [
    {}
  ],
  "respondentId": "string",
  "respondentName": "string",
  "surveyName": "string",
  "surveyUUID": "string",
  "userEmails": [
    "string"
  ],
  "userIds": [
    "string"
  ],
  "deepLink" : "String",
  "appName"  : "String"
}
```



