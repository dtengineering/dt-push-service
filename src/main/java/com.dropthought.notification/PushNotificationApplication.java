package com.dropthought.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PushNotificationApplication {
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(PushNotificationApplication.class, args);
    }

}