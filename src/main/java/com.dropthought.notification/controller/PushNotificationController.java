package com.dropthought.notification.controller;

import com.dropthought.notification.model.PushNotification;
import com.dropthought.notification.service.PushNotificationService;
import com.dropthought.notification.util.Utils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/notification/push")
public class PushNotificationController {

    private static final Logger logger = LoggerFactory.getLogger(PushNotificationController.class);

    /**
     * Http Headers
     */
    protected final HttpHeaders httpHeaders = new HttpHeaders();

    @Autowired
    PushNotificationService pushNotificationService;

    /**
     * returns the version of the service
     *
     * @return Map(version, major.minor.patch)
     */
    @GetMapping("version")
    public Map<String, String> version() {
        return Collections.singletonMap("VERSION", "1.0.0");
    }

    /**
     * Send mobile push notification to respective account email
     *
     * @param pushNotification
     * @return
     */
    @PostMapping("/{environment}")
    @ResponseBody
    public ResponseEntity<String> notify(HttpServletRequest request,
                                         @PathVariable(value = "environment") String environment,
                                         @RequestBody @Valid PushNotification pushNotification) {
        logger.info("Send push notification api starts");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        Map<String, Object> resultMap = new HashMap<>();
        try {
            // Call push notification API
            Map<String, Object> response = pushNotificationService.sendPushNotifications(pushNotification, environment);
            if (Boolean.TRUE.equals(response.get("success"))) {
                return ResponseEntity.ok().headers(httpHeaders).body(new JSONObject(response).toString());
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(httpHeaders).body(new JSONObject(resultMap).toString());
            }
        } catch (Exception e) {
            logger.error("Error occurred while sending push notification", e);
            resultMap.put("success", false);
            resultMap.put("error", "Internal Server Error. Please try again later.");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).headers(httpHeaders).body(new JSONObject(resultMap).toString());
        }
    }

}
