package com.dropthought.notification.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class PushNotification {

    @NotEmpty(message = "Recipient list ('to') must not be empty.")
    private List<String> emails;

    private String title;

    private Map<String, String> data; // optional, used to send additional data with notification payload & deeplink to select tab in mobile

    private String message;

    @NotNull(message = "App name must not be null or empty")
    @NotEmpty(message = "App name must not be null or empty")
    private String appName;

}
