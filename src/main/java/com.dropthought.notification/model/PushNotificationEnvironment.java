package com.dropthought.notification.model;


public enum PushNotificationEnvironment {
    //non-prod
    STAGE("staging"),
    TEST("dev"),
    AUTOMATION("dev"),

    //pre-prod
    SANDBOX("prod"),
    DEMO("demo"),

    //prod
    PROD("prod"),
    //default
    DEFAULT("dev");

    private final String mappedValue;

    PushNotificationEnvironment(String mappedValue) {
        this.mappedValue = mappedValue;
    }

    public static String getMappedMobileUrl(String environment) {
        if (environment == null || environment.isEmpty()) {
            return DEFAULT.mappedValue;
        }
        try {
            return PushNotificationEnvironment.valueOf(environment.toUpperCase()).mappedValue;
        } catch (IllegalArgumentException ex) {
            return DEFAULT.mappedValue;
        }
    }
}
