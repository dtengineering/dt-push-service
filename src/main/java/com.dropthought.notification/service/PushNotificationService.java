package com.dropthought.notification.service;

import com.dropthought.notification.model.PushNotification;
import com.dropthought.notification.model.PushNotificationEnvironment;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;


@Component
public class PushNotificationService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${x-api-key}")
    private String apiKey;

    @Value("${dt.pushnotification.url}")
    private String pushNotificationUrl;

    @Autowired
    RestTemplate restTemplate;

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    /**
     * Method to manage pushNotifications and sending to FCM
     *
     * @param pushNotificationBean
     * @param environment
     */
    public Map sendPushNotifications(PushNotification pushNotificationBean, String environment) {
        logger.info("Start sendPushNotifications");
        Map<String, Object> payload = mapper.convertValue(pushNotificationBean, new TypeReference<Map>() {
        });
        logger.info("Environment {}", environment);
        String pushNotificationUrl = getPushNotificationUrlByEnvironment(environment);
        return callPushNotificationRestApi(pushNotificationUrl, new JSONObject(payload).toString());
    }

    private String getPushNotificationUrlByEnvironment(String environment) {
        String url = pushNotificationUrl;
        String env = PushNotificationEnvironment.getMappedMobileUrl(environment);

        if (env != null && !env.isEmpty()) {
            url = url.replace("environment", env);
        }

        return url;
    }

    /**
     * Method to call pushNotificationRestApi
     *
     * @param inputJson
     * @return
     */
    private Map callPushNotificationRestApi(String uri, String inputJson) {

        Map responseMap = new HashMap();
        logger.info("payload {}", inputJson);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("x-api-key", apiKey);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        try {

            logger.info("uri {}", uri);
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("PushNotification request sent successfully");
                    responseMap = (Map) responseEntity.getBody();
                }
            } else if(responseEntity != null && responseEntity.getStatusCode() != HttpStatus.BAD_REQUEST){
                responseMap.put("error", "Internal Server Error");
                responseMap.put("message", responseEntity.getBody());
                responseMap.put("success", false);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            responseMap.put("error", "Internal Server Error");
            responseMap.put("success", false);
            responseMap.put("message", e.getMessage());
        }
        logger.info("response {}", new JSONObject(responseMap));
        return responseMap;
    }
}