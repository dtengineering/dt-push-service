package com.dropthought.notification.util;

public class Utils {

    /**
     * Function to check given object is null
     *
     * @param obj
     * @return
     */
    public static Boolean isNull(Object obj) {
        return (obj == null) ? true : false;
    }

    /**
     * Function to check given object is not null
     *
     * @param obj
     * @return
     */
    public static Boolean isNotNull(Object obj) {
        return (obj != null) ? true : false;
    }

    /**
     * Function to check given string is not empty
     *
     * @param input
     * @return
     */
    public static Boolean notEmptyString(String input) {
        Boolean value = false;
        try {
            if (input != null && input.length() > 0) {
                value = true;
            }
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return value;
    }

    /**
     * Function to check if a string is empty or null
     *
     * @param input
     * @return
     */
    public static Boolean emptyString(String input) {
        Boolean value = false;
        try {
            input = input.trim();
            if (input == null || input.length() == 0) {
                value = true;
            }
        } catch (Exception e) {
            value = true;
        }
        return value;
    }

}
