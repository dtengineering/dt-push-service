package com.dropthought.notification.controller;

import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@Configuration
@ComponentScan("com.dropthought.portal.controller")
@TestPropertySource("classpath:test-config.properties")
public class PushNotificationControllerTest {

}
